@extends('layouts.dashboard')

@section('title')
    Dashboard
@endsection

@section('head')
<h5 class="card-title">Dashboard - {{$label}}</h5>
@endsection

@section('sidebar')
<div class="sidebar" data-color="dark-blue">
      
    <div class="logo text-white">
        MONITOREO BOTNETS
    </div>

    <div class="sidebar-wrapper" id="sidebar-wrapper">
      <ul class="nav">
        <li class="active">
          <a href="/">
            <i class="now-ui-icons design_app"></i>
            <p>Dashboard</p>
          </a>
        </li>
        <li>
          <a href="/botnets">
            <i class="now-ui-icons education_atom"></i>
            <p>Reporte Botnets</p>
          </a>
        </li>
        <li>
          <a href="/dominios">
            <i class="now-ui-icons objects_spaceship"></i>
            <p>Top Dominios</p>
          </a>
        </li>
        <li>
          <a href="/alertas">
            <i class="now-ui-icons ui-1_bell-53"></i>
            <p>Alertas</p>
          </a>
        </li>
        <li>
          <a href="/lb">
            <i class="now-ui-icons design_bullet-list-67"></i>
            <p>Lista Blanca</p>
          </a>
        </li>
        <li>
            <a href="/ln">
              <i class="now-ui-icons design_bullet-list-67"></i>
              <p>Lista Negra</p>
            </a>
        </li>
          
          <!-- Authentication Links -->
          @guest
          <li class="nav-item">
              <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
          </li>
          @if (Route::has('register'))
              <li class="nav-item">
                  <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
              </li>
          @endif
      @else
          <li class="nav-item dropdown">
              <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                  {{ Auth::user()->name }}
              </a>

              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="{{ route('logout') }}"
                     onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
                      {{ __('Logout') }}
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>
              </div>
          </li>
      @endguest

      </ul>
    </div>
  </div>
@endsection

@section('graf_total')
<div class="col-md-8">
  <canvas id="lineChartDomains"></canvas>
</div>
@endsection

@section('graf_botnets')
<div class="card-body">
  <div class="chart-area">
    <canvas id="lineChartBots"></canvas>
  </div>
</div>
@endsection

@section('graf_listaNegra')
<div class="card-body">
  <div class="chart-area">
    <canvas id="lineChart_LN"></canvas>
  </div>
</div>
@endsection

@section('graf_listaBlanca')
<div class="card-body">
  <div class="chart-area">
    <canvas id="lineChart_LB"></canvas>
  </div>
</div>
@endsection

@section('footer')
<footer class="footer">
  <div class="container-fluid">
    <div class="copyright text-muted" id="copyright" >
      &copy; <script>
        document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
      </script> Dennis Vargas.
    </div>
  </div>
</footer>
@endsection


@section('scripts')
<!--   Core JS Files   -->
<script src="../assets/js/core/jquery.min.js"></script>
<script src="../assets/js/core/popper.min.js"></script>
<script src="../assets/js/core/bootstrap.min.js"></script>
<script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!-- Chart JS -->
<script src="../assets/js/plugins/chartjs.min.js"></script>
<script src="../assets/js/graficas.js"></script>
<!--  Notifications Plugin    -->
<script src="../assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/now-ui-dashboard.min.js?v=1.5.0" type="text/javascript"></script>  


<script type="text/javascript">
  //var dns = {!! json_encode($total_dns) !!};
  var dns = @json($total_dns);
  //console.log(dns);
  var labels = dns.map(function (e) {
    return e.fecha_hr;
  });
  var values = dns.map(function (e) {
    return e.cantidad;
  });
  chartTotal(labels, values);

  var bots = @json($botnets);
  //console.log(bots);
  var labels = bots.map(function (e) {
    return e.fecha_hr;
  });
  var values = bots.map(function (e) {
    return e.cantidad;
  });
  chartBots(labels, values);

  var ln = @json($lista_negra);
  //console.log(ln);
  var labels = ln.map(function (e) {
    return e.fecha_hr;
  });
  var values = ln.map(function (e) {
    return e.cantidad;
  });
  chartLN(labels, values);

  var lb = @json($lista_blanca);
  //console.log(lb);
  var labels = lb.map(function (e) {
    return e.fecha_hr;
  });
  var values = lb.map(function (e) {
    return e.cantidad;
  });
  chartLB(labels, values);
</script>
@endsection