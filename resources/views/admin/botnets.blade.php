@extends('layouts.plantilla')

@section('header')
  <title>
    Botnets
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!-- CSS Files -->
  <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="../assets/css/now-ui-dashboard.css?v=1.5.0" rel="stylesheet" />
  <link rel="stylesheet" href="../plugins/datatables/css/dataTables.bootstrap4.min.css">
  <!-- google icons -->
  <link rel="stylesheet" href="../plugins/icons.css">
@endsection


@section('label')
  <h5 class="card-title">Reporte de Botnets - {{$label}}</h5>
@endsection

@section('range_menu')
<a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
  <i class="now-ui-icons business_chart-bar-32"></i>
  <p>
    <span class="d-md-block">Rango</span>
  </p>
</a>
<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
  <a class="dropdown-item" href="{{ action('BotnetController@botnets_1hr') }}">Última Hora [ 1hr ]</a>
  <a class="dropdown-item" href="{{ action('BotnetController@botnets_7days') }}">Últimos 7 días</a>
  <a class="dropdown-item" href="{{ action('BotnetController@botnets_30days') }}">Úlimos 30 días</a>
</div>
@endsection

@section('sidebar')
<div class="sidebar" data-color="dark-blue">
      
    <div class="logo text-white">
        MONITOREO BOTNETS
    </div>

    <div class="sidebar-wrapper" id="sidebar-wrapper">
      <ul class="nav">
        <li>
          <a href="/">
            <i class="now-ui-icons design_app"></i>
            <p>Dashboard</p>
          </a>
        </li>
        <li class="active">
          <a href="/botnets">
            <i class="now-ui-icons education_atom"></i>
            <p>Reporte Botnets</p>
          </a>
        </li>
        <li>
          <a href="/dominios">
            <i class="now-ui-icons objects_spaceship"></i>
            <p>Top Dominios</p>
          </a>
        </li>
        <li>
          <a href="/alertas">
            <i class="now-ui-icons ui-1_bell-53"></i>
            <p>Alertas</p>
          </a>
        </li>
        <li>
          <a href="/lb">
            <i class="now-ui-icons design_bullet-list-67"></i>
            <p>Lista Blanca</p>
          </a>
        </li>
        <li>
            <a href="/ln">
              <i class="now-ui-icons design_bullet-list-67"></i>
              <p>Lista Negra</p>
            </a>
        </li>
          
          <!-- Authentication Links -->
          @guest
          <li class="nav-item">
              <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
          </li>
          @if (Route::has('register'))
              <li class="nav-item">
                  <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
              </li>
          @endif
      @else
          <li class="nav-item dropdown">
              <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                  {{ Auth::user()->name }}
              </a>

              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="{{ route('logout') }}"
                     onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
                      {{ __('Logout') }}
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>
              </div>
          </li>
      @endguest

      </ul>
    </div>
  </div>
@endsection


@section('content')
<div class="card-header">
</div>
<div class="container" mb-1 mt-1>
    <div class="table-responsive">
      <table class="table table-striped table-bordered databotnets" style="width: 100%">
        <thead class="text-primary">
          <tr>
            <th>
              IP ORIGEN
            </th>
            <th>
              FECHA-HORA
            </th>
            <th>
              REGISTRO DNS
            </th>
            <th>
              DOMINIO
            </th>
            <th>
              DESCRIPCION
            </th>
          </tr>
        </thead>

        <tbody>
          @if($total_botnets)
            @foreach($total_botnets as $item)
              <tr>
                <td>
                  {{ $item->ip_dst }}
                </td>
                <td>
                  {{ $item->fecha_hr }}
                </td>
                <td>
                  {{ $item->tipo_query }}
                </td>
                <td>
                  {{ $item->dominio }}
                </td>
                <td>
                  {{ $item->descripcion }}
                </td>
              </tr>
            @endforeach
            @else 
            <p>Sin Datos en {{$label}}</p>
          @endif
        </tbody>

        <tfoot>
          <tr>
            <th>
              IP ORIGEN
            </th>
            <th>
              FECHA-HORA
            </th>
            <th>
              REGISTRO DNS
            </th>
            <th>
              DOMINIO
            </th>
            <th>
              DESCRIPCION
            </th>
          </tr>
        </tfoot>
      </table>
    </div>
</div>
@endsection

@section('scripts')
  <!--   Core JS Files   -->
  <script src="../assets/js/core/jquery.min.js"></script>
  <script src="../assets/js/core/popper.min.js"></script>
  <script src="../assets/js/core/bootstrap.min.js"></script>
  <script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>

  <script src="../plugins/datatables/js/jquery.dataTables.min.js"></script>
  <script src="../plugins/datatables/js/dataTables.bootstrap4.min.js"></script>

  <!--  Notifications Plugin    -->
  <script src="../assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="../assets/js/now-ui-dashboard.min.js?v=1.5.0" type="text/javascript"></script>  

  <script>
    $('.databotnets').dataTable({
      order: [[1, 'desc']],
      pagingType: 'full_numbers',
      "language": {
            "url": "../plugins/datatables/Spanish.json"
        }
    });
  </script>
@endsection