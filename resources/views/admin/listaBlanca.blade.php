@extends('layouts.plantilla')

@section('header')
  <title>
    Lista Blanca
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!-- CSS Files -->
  <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="../assets/css/now-ui-dashboard.css?v=1.5.0" rel="stylesheet" />
  <link rel="stylesheet" href="../plugins/datatables/css/dataTables.bootstrap4.min.css">
  <!-- google icons -->
  <link rel="stylesheet" href="../plugins/icons.css">
@endsection

@section('label')
<h5 class="card-title">Lista Blanca</h5>
@endsection

@section('range_menu')
@endsection

@section('sidebar')
  <div class="sidebar" data-color="dark-blue">
        
    <div class="logo text-white">
        MONITOREO dominios
    </div>

    <div class="sidebar-wrapper" id="sidebar-wrapper">
      <ul class="nav">
        <li>
          <a href="/">
            <i class="now-ui-icons design_app"></i>
            <p>Dashboard</p>
          </a>
        </li>
        <li>
          <a href="/botnets">
            <i class="now-ui-icons education_atom"></i>
            <p>Reporte Botnets</p>
          </a>
        </li>
        <li>
          <a href="/dominios">
            <i class="now-ui-icons objects_spaceship"></i>
            <p>Top Dominios</p>
          </a>
        </li>
        <li>
          <a href="/alertas">
            <i class="now-ui-icons ui-1_bell-53"></i>
            <p>Alertas</p>
          </a>
        </li>
        <li class="active">
          <a href="/lb">
            <i class="now-ui-icons design_bullet-list-67"></i>
            <p>Lista Blanca</p>
          </a>
        </li>
        <li>
            <a href="/ln">
              <i class="now-ui-icons design_bullet-list-67"></i>
              <p>Lista Negra</p>
            </a>
        </li>
          
          <!-- Authentication Links -->
          @guest
          <li class="nav-item">
              <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
          </li>
          @if (Route::has('register'))
              <li class="nav-item">
                  <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
              </li>
          @endif
      @else
          <li class="nav-item dropdown">
              <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                  {{ Auth::user()->name }}
              </a>

              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                                  document.getElementById('logout-form').submit();">
                      {{ __('Logout') }}
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>
              </div>
          </li>
      @endguest

      </ul>
    </div>
  </div>
@endsection


@section('content')

<div class="card-header">
</div>
<div class="container" mb-1 mt-1>
    <div class="table-responsive">
      <table class="table table-striped table-bordered dataLB" style="width: 90%">
        <thead class="text-primary">
          <tr>
            <th>
              DOMINIO
            </th>
            <th>
              DESCRIPCION
            </th>
            <th>
              FECHA_HR
            </th>
            <th>
              <button type="button" class="boton add" data-title="Adicionar" data-toggle="modal" data-target="#adicionar">
                + Adicionar &nbsp;
              </button>
            </th>
          </tr>
        </thead>

        <tbody>
          @if($lista_blanca)
            @foreach($lista_blanca as $item)
              <tr>
                <td>
                  {{ $item->dominio }}
                </td>
                <td>
                  {{ $item->descripcion }}
                </td>
                <td>
                  {{ $item->fecha_hr }}
                </td>
                <td>
                  <button type="button" class="boton" id="boton-editar" title="Editar" data-title="Editar" data-toggle="modal" data-target="#editar" 
                  data-id="{{$item->id_lb}}" data-dominio="{{$item->dominio}}" data-fecha="{{$item->fecha_hr}}" data-desc="{{$item->descripcion}}">
                    <i class="material-icons blue">edit</i>
                  </button>
                  <button type="button" class="boton" id="boton-borrar" title="Borrar" data-title="Borrar" data-toggle="modal" data-target="#borrar" 
                  data-id="{{$item->id_lb}}" data-dominio="{{$item->dominio}}">
                    <i class="material-icons red">delete</i>
                  </button>
                  <button type="button" class="boton" id="boton-mover" title="Mover a lista negra" data-title="Mover" data-toggle="modal" data-target="#mover" 
                  data-id="{{$item->id_lb}}" data-dominio="{{$item->dominio}}" data-fecha="{{$item->fecha_hr}}" data-desc="{{$item->descripcion}}">
                    <i class="material-icons yellow">sync_alt</i>
                  </button>
                </td>
              </tr>
            @endforeach
            @else 
            <p>Sin Datos en {{$label}}</p>
          @endif
        </tbody>

        <tfoot>
          <tr>
            <th>
              DOMINIO
            </th>
            <th>
              DESCRIPCION
            </th>
            <th>
              FECHA_HR
            </th>
            <th>
            </th>
          </tr>
        </tfoot>
      </table>
    </div>
</div>

<div class="container">
  <!--   Add   -->
  <div class="modal fade" id="adicionar" tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <form id="form-add" method="POST" action="{{ url('addLB')}}">
        @csrf
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">
            <i class="material-icons">close</i>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="dominio">Dominio:</label>
            <input id="add-dominio" name="add_dominio" type="text" class="form-control" maxlength="199" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="desc">Descripción:</label>
            <input id="add-desc" name="add_desc" type="text" class="form-control" maxlength="99" autocomplete="off">
          </div>
        </div>
        <div class="modal-footer">
            <button type="button submit" class="btn btn-success btn-sm btn-block">
              <h5><i class="material-icons">check</i>Adicionar</h5>
            </button>
        </div>
      </div>
      </form>
    </div>
  </div>
  <!--   Edit   -->
  <div class="modal fade" id="editar" tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <form id="form-edit" method="POST" action="{{ url('editLB')}}">
        @csrf
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">
              <i class="material-icons">close</i>
            </button>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <input type="hidden" id="id-lb" name="id_lb" value="">
              <label for="dominio">Dominio:</label>
              <input id="dominio" type="text" class="form-control" value="" disabled>
            </div>
            <div class="form-group">
              <label for="fecha">Fecha:</label>
              <input id="fecha" type="text" class="form-control" value="" disabled>
            </div>
            <div class="form-group">
              <label for="desc">Descripción:</label>
              <input id="desc" type="text" name="desc" class="form-control" value="" maxlength="99">
            </div>
          </div>
          <div class="modal-footer">
              <button type="button submit" class="btn btn-warning btn-sm btn-block">
                <h5><i class="material-icons">check</i>Actualizar</h5>
              </button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <!--   Delete   -->
  <div class="modal fade" id="borrar" tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <form id="form-delete" method="POST" action="{{ url('delLB')}}">
        @csrf
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">
              <i class="material-icons">close</i>
            </button>
          </div>
          <div class="modal-body">
            <input type="hidden" id="id_lb" name="id_lb" value="">
            <div class="alert alert-danger">
              <i class="material-icons">warning</i>
              Confirma borrar el registro
              &nbsp;<span id="del-dominio"></span> &nbsp;?</div>
          </div>
          <div class="modal-footer ">
            <button type="button submit" class="btn btn-success" ><i class="material-icons">check</i>Si</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="material-icons">cancel</i>No</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <!--   Move   -->
  <div class="modal fade" id="mover" tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <form id="form-edit" method="POST" action="{{ url('moveLB')}}">
        @csrf
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">
              <i class="material-icons">close</i>
            </button>
          </div>
          <div class="modal-body">
              <input type="hidden" id="id_lb" name="id_lb" value="">
              <div class="alert alert-warning">
                <i class="material-icons">warning</i>
                Confirma mover a Lista Negra
                &nbsp;<span id="mov-dominio"></span> &nbsp;?</div>
          </div>
          <div class="modal-footer ">
            <button type="button submit" class="btn btn-success" ><i class="material-icons">check</i>Si</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="material-icons">cancel</i>No</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <!--   end container   -->
</div>
@endsection

@section('scripts')
  <!--   Core JS Files   -->
  <script src="../assets/js/core/jquery.min.js"></script>
  <script src="../assets/js/core/popper.min.js"></script>
  <script src="../assets/js/core/bootstrap.min.js"></script>
  <script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>

  <script src="../plugins/datatables/js/jquery.dataTables.min.js"></script>
  <script src="../plugins/datatables/js/dataTables.bootstrap4.min.js"></script>

  <!--  Notifications Plugin    -->
  <script src="../assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="../assets/js/now-ui-dashboard.min.js?v=1.5.0" type="text/javascript"></script>  
  
  <script>
    $('.dataLB').dataTable({
      "ordering": false,
      pagingType: 'full_numbers',
      "language": {
            "url": "../plugins/datatables/Spanish.json"
        }
    });
  </script>

  <script>
    $(document).on("click", "#boton-editar", function () {
     var id_lb = $(this).data('id');
     var dominio = $(this).data('dominio');
     var fecha = $(this).data('fecha');
     var desc = $(this).data('desc');
     $(".modal-body #id-lb").val(id_lb);
     $(".modal-body #dominio").val(dominio);
     $(".modal-body #fecha").val(fecha);
     $(".modal-body #desc").val(desc);
    });

    $(document).on("click", "#boton-borrar", function () {
     var id_lb = $(this).data('id');
     var dominio = $(this).data('dominio');
     $(".modal-body #id_lb").val(id_lb);
     $("#del-dominio").text("\""+dominio+"\"");
    });

    $(document).on("click", "#boton-mover", function () {
     var id_lb = $(this).data('id');
     var dominio = $(this).data('dominio');
     $(".modal-body #id_lb").val(id_lb);
     $("#mov-dominio").text("\""+dominio+"\"");
    });
  </script>
  <script type="text/javascript">
    {{ Session::get('msg') ?? '' }}
    try{
      var id_msg = {{ Session::get('msg') }};
      console.log('msg: '+id_msg);
      if(id_msg!=null){
        switch (id_msg){
          case 1:
            //add
            $.notify({
              message: "<h6><i class=\"material-icons\">check</i>Registro Agregado Exitosamente</h6>"
              },{
                type: 'success'
            });
            break;
          case 2:
            //update
            $.notify({
              message: "<h6><i class=\"material-icons\">check</i>Registro Editado Exitosamente</h6>"
              },{
                type: 'warning'
            });
            break;
          case 3:
            //delete
            $.notify({
              message: "<h6><i class=\"material-icons\">check</i>Registro Eliminado</h6>"
              },{
                type: 'danger'
            });
            break;
          case 4:
            //move
            $.notify({
              message: "<h6><i class=\"material-icons\">check</i>Registro llevado a Lista Negra</h6>"
              },{
                type: 'info'
            });
            break;
          case 5:
            //error
            $.notify({
              message: "<h6><i class=\"material-icons\">check</i>Error</h6>"
              },{
                type: 'warning'
            });
            break;


          default:
            console.log('No messages');
        }
      }
      {{ Session::put('msg','0') }};
    }
    catch(error) {
     console.error(error);
    }
  </script>

@endsection