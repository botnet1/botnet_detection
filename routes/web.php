<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});
*/

Auth::routes();
Route::get('/', 'HomeController@dashboard')->name('dashboard')->middleware('auth');
Route::get('last-7days', 'HomeController@dashboard_7days')->name('dashboard')->middleware('auth');
Route::get('last-30days', 'HomeController@dashboard_30days')->name('dashboard')->middleware('auth');

Route::get('botnets', 'BotnetController@botnets_1hr')->name('botnets')->middleware('auth');
Route::get('botnets-7days', 'BotnetController@botnets_7days')->name('botnets')->middleware('auth');
Route::get('botnets-30days', 'BotnetController@botnets_30days')->name('botnets')->middleware('auth');

Route::get('dominios', 'DominioController@dominios_1hr')->name('topdominios')->middleware('auth');
Route::get('dominios-7days', 'DominioController@dominios_7days')->name('topdominios')->middleware('auth');
Route::get('dominios-30days', 'DominioController@dominios_30days')->name('topdominios')->middleware('auth');


Route::get('alertas', 'AlertaController@alertas_24hrs')->name('alertas')->middleware('auth');
Route::get('alertas-7days', 'AlertaController@alertas_7days')->name('alertas')->middleware('auth');
Route::get('alertas-30days', 'AlertaController@alertas_30days')->name('alertas')->middleware('auth');

Route::get('lb', 'ListaBlancaController@getListaBlanca')->name('listaBlanca')->middleware('auth');
Route::post('addLB','ListaBlancaController@addLB')->name('addListaBlanca')->middleware('auth');
Route::post('editLB','ListaBlancaController@editLB')->name('editListaBlanca')->middleware('auth');
Route::post('delLB','ListaBlancaController@delLB')->name('delListaBlanca')->middleware('auth');
Route::post('moveLB','ListaBlancaController@moveLB')->name('moveListaBlanca')->middleware('auth');

Route::get('ln', 'ListaNegraController@getListaNegra')->name('ListaNegra')->middleware('auth');
Route::post('addLN','ListaNegraController@addLN')->name('addListaNegra')->middleware('auth');
Route::post('editLN','ListaNegraController@editLN')->name('editListaNegra')->middleware('auth');
Route::post('delLN','ListaNegraController@delLN')->name('delListaNegra')->middleware('auth');
Route::post('moveLN','ListaNegraController@moveLN')->name('moveListaNegra')->middleware('auth');


/*
Route::get('/admin', function () {
    return view('admin.dashboard');
});
*/

