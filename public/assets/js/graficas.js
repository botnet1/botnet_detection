//line
function chartTotal(labels, values){
    var ctxL = document.getElementById("lineChartDomains").getContext('2d');
    var myLineChart = new Chart(ctxL, {
    type: 'line',
    data: {
    //labels: ["January", "February", "March", "April", "May", "June", "July"],
    labels: labels,
    datasets: [{
    label: "TOTAL QUERYS DNS ANALIZADOS",
    //data: [280, 480, 400, 190, 860, 270, 900],
    data: values,
    backgroundColor: [
    'rgba(255,255,255, .3)',
    ],
    borderColor: [
    'rgba(173,216,230, .7)',
    ],
    borderWidth: 2
    },
    ]
    },
    options: {
    responsive: true
    }
    });
    return myLineChart;
}

function chartBots(labels, values){
    var ctxL = document.getElementById("lineChartBots").getContext('2d');
    var myLineChart = new Chart(ctxL, {
    type: 'line',
    data: {
    labels: labels,
    datasets: [{
    label: "N° Consultas",
    data: values,
    backgroundColor: [
    'rgba(255, 0, 0, .2)',
    ],
    borderColor: [
    'rgba(255, 0, 0, .7)',
    ],
    borderWidth: 2
    },
    ]
    },
    options: {
    responsive: true
    }
    });
    return myLineChart;
}

function chartLN(labels, values){
    var ctxL = document.getElementById("lineChart_LN").getContext('2d');
    var myLineChart = new Chart(ctxL, {
    type: 'line',
    data: {
    labels: labels,
    datasets: [{
    label: "N° Consultas",
    data: values,
    backgroundColor: [
    'rgba(255, 140, 0, .2)',
    ],
    borderColor: [
    'rgba(255, 140, 0, .7)',
    ],
    borderWidth: 2
    },
    ]
    },
    options: {
    responsive: true
    }
    });
    return myLineChart;
}

function chartLB(labels, values){
    var ctxL = document.getElementById("lineChart_LB").getContext('2d');
    var myLineChart = new Chart(ctxL, {
    type: 'line',
    data: {
    labels: labels,
    datasets: [{
    label: "N° Consultas",
    data: values,
    backgroundColor: [
    'rgba(0, 255, 0, .2)',
    ],
    borderColor: [
    'rgba(0, 255, 0, .7)',
    ],
    borderWidth: 2
    },
    ]
    },
    options: {
    responsive: true
    }
    });
    return myLineChart;
}