<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListaBlanca extends Model
{
    //
    protected $table='lista_blanca';
    protected $fillable=['dominio','descripcion', 'fecha_hr'];
    protected $primaryKey='id_lb';
    protected $hidden=['id_lb'];
    public $timestamps = false;

}
