<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\ListaBlanca;
use App\ListaNegra;
use Carbon\Carbon;
use phpDocumentor\Reflection\Types\Null_;

class ListaBlancaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getListaBlanca(Request $request)
    {
        #dd($request->session()->all());
        $lista_blanca = DB::table('lista_blanca')->get();
        return view('admin.listaBlanca', compact('lista_blanca'));
    }

    public function addLB(Request $request)
    {
        #dd($request->all());
        $lb = new ListaBlanca();
        $lb->dominio = $request->add_dominio;
        $lb->descripcion = $request->add_desc;
        $lb->fecha_hr = Carbon::now();
        $lb->save();
        $request->session()->put('msg', '1'); //usando un valor en la session en lugar de pasarlo por parametro
        return redirect()->action('ListaBlancaController@getListaBlanca');
    }

    public function editLB(Request $request)
    {
        #dd($request->all());
        $id_lb=$request->id_lb;
        $desc=$request->desc;
        DB::table('lista_blanca')
            ->where('id_lb', $id_lb)
            ->update(['descripcion' => $desc]);
        $request->session()->put('msg', '2');
        return redirect()->action('ListaBlancaController@getListaBlanca');
    }

    public function delLB(Request $request)
    {
        #dd($request->all());
        $id_lb=$request->id_lb;
        DB::table('lista_blanca_registros')->where('id_lb', '=', $id_lb)->delete();
        DB::table('lista_blanca')->where('id_lb', '=', $id_lb)->delete();
        $request->session()->put('msg', '3');
        return redirect()->action('ListaBlancaController@getListaBlanca');
    }

    public function moveLB(Request $request)
    {
        #dd($request->all());
        $id_lb=$request->id_lb;
        $lista_blanca = DB::table('lista_blanca')->where('id_lb', $id_lb)->get();
        if($lista_blanca){
            DB::table('lista_blanca_registros')->where('id_lb', '=', $id_lb)->delete();
            DB::table('lista_blanca')->where('id_lb', '=', $id_lb)->delete();
            $ln = new ListaNegra();
            $ln->dominio = $lista_blanca[0]->dominio;
            $ln->descripcion = $lista_blanca[0]->descripcion;
            $ln->fecha_hr = Carbon::now();
            $ln->save();
            $request->session()->put('msg', '4');
            return redirect()->action('ListaBlancaController@getListaBlanca');
        }
        else{
            $request->session()->put('msg', '5'); //error
            return redirect()->action('ListaBlancaController@getListaBlanca');
        }
    }
}
