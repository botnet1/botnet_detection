<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AlertaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function alertas_24hrs()
    {
        $total_alertas = DB::select('
        SELECT fecha_hr, ipcli, descripcion, status 
        FROM alertas 
        WHERE fecha_hr >= DATE_SUB(NOW(), INTERVAL 1 DAY) 
        ');

        $label="Últimas 24Horas [24hr]";
        return view('admin.alertas', compact('total_alertas', 'label'));
    }

    public function alertas_7days()
    {
        $total_alertas = DB::select('
        SELECT fecha_hr, ipcli, descripcion, status 
        FROM alertas 
        WHERE fecha_hr >= DATE_SUB(NOW(), INTERVAL 7 DAY) 
        ');

        $label="Últimos 7 días";
        return view('admin.alertas', compact('total_alertas', 'label'));
    }

    public function alertas_30days()
    {
        $total_alertas = DB::select('
        SELECT fecha_hr, ipcli, descripcion, status 
        FROM alertas 
        WHERE fecha_hr >= DATE_SUB(NOW(), INTERVAL 30 DAY) 
        ');

        $label="Últimos 30 días";
        return view('admin.alertas', compact('total_alertas', 'label'));
    }
}
