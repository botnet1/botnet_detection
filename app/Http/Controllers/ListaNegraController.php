<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\ListaNegra;
use App\ListaBlanca;
use Carbon\Carbon;
use phpDocumentor\Reflection\Types\Null_;

class ListaNegraController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getListaNegra(Request $request)
    {
        #dd($request->session()->all());
        $lista_negra = DB::table('lista_negra')->get();
        return view('admin.listaNegra', compact('lista_negra'));
    }

    public function addLN(Request $request)
    {
        #dd($request->all());
        $ln = new ListaNegra();
        $ln->dominio = $request->add_dominio;
        $ln->descripcion = $request->add_desc;
        $ln->fecha_hr = Carbon::now();
        $ln->save();
        $request->session()->put('msg', '1'); //usando un valor en la session en lugar de pasarlo por parametro
        return redirect()->action('ListaNegraController@getListaNegra');
    }

    public function editLN(Request $request)
    {
        #dd($request->all());
        $id_ln=$request->id_ln;
        $desc=$request->desc;
        DB::table('lista_negra')
            ->where('id_ln', $id_ln)
            ->update(['descripcion' => $desc]);
        $request->session()->put('msg', '2');
        return redirect()->action('ListaNegraController@getListaNegra');
    }

    public function delLN(Request $request)
    {
        #dd($request->all());
        $id_ln=$request->id_ln;
        DB::table('lista_negra_registros')->where('id_ln', '=', $id_ln)->delete();
        DB::table('lista_negra')->where('id_ln', '=', $id_ln)->delete();
        $request->session()->put('msg', '3');
        return redirect()->action('ListaNegraController@getListaNegra');
    }

    public function moveLN(Request $request)
    {
        #dd($request->all());
        $id_ln=$request->id_ln;
        $lista_negra = DB::table('lista_negra')->where('id_ln', $id_ln)->get();
        if($lista_negra){
            DB::table('lista_negra_registros')->where('id_ln', '=', $id_ln)->delete();
            DB::table('lista_negra')->where('id_ln', '=', $id_ln)->delete();
            $lb = new ListaBlanca();
            $lb->dominio = $lista_negra[0]->dominio;
            $lb->descripcion = $lista_negra[0]->descripcion;
            $lb->fecha_hr = Carbon::now();
            $lb->save();
            $request->session()->put('msg', '4');
            return redirect()->action('ListaNegraController@getListaNegra');
        }
        else{
            $request->session()->put('msg', '5'); //error
            return redirect()->action('ListaNegraController@getListaNegra');
        }
    }
}
