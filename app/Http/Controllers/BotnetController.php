<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class BotnetController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function botnets_1hr()
    {
        $total_botnets = DB::select('
        SELECT lnr.ip_dst, lnr.fecha_hr, lnr.tipo_query, ln.dominio, ln.descripcion 
        FROM lista_negra_registros as lnr
        INNER JOIN lista_negra as ln
        ON lnr.id_ln=ln.id_ln 
        AND lnr.fecha_hr >= DATE_SUB(NOW(), INTERVAL 1 HOUR) 
        AND ln.descripcion like "Botnet%" 
        ORDER BY lnr.fecha_hr');

        $label="Última Hora [1hr]";
        return view('admin.botnets', compact('total_botnets', 'label'));
    }

    public function botnets_7days()
    {
        $total_botnets = DB::select('
        SELECT lnr.ip_dst, lnr.fecha_hr, lnr.tipo_query, ln.dominio, ln.descripcion 
        FROM lista_negra_registros as lnr
        INNER JOIN lista_negra as ln
        ON lnr.id_ln=ln.id_ln 
        AND lnr.fecha_hr >= DATE_SUB(NOW(), INTERVAL 7 DAY) 
        AND ln.descripcion like "Botnet%" 
        ORDER BY lnr.fecha_hr');

        $label="Últimos 7 días";
        return view('admin.botnets', compact('total_botnets', 'label'));
    }

    public function botnets_30days()
    {
        $total_botnets = DB::select('
        SELECT lnr.ip_dst, lnr.fecha_hr, lnr.tipo_query, ln.dominio, ln.descripcion 
        FROM lista_negra_registros as lnr
        INNER JOIN lista_negra as ln
        ON lnr.id_ln=ln.id_ln 
        AND lnr.fecha_hr >= DATE_SUB(NOW(), INTERVAL 30 DAY) 
        AND ln.descripcion like "Botnet%" 
        ORDER BY lnr.fecha_hr');

        $label="Últimos 30 días";
        return view('admin.botnets', compact('total_botnets', 'label'));
    }
}
