<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class DominioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function dominios_1hr()
    {
        $total_dominios = DB::select('
        SELECT d.dominio, r.tipo_query, count(*) as cantidad
        FROM registros_dns_query as r 
        INNER JOIN dominios_dataset as d 
        ON r.id_dominio=d.id_dominio 
        AND r.fecha_hr >= DATE_SUB(NOW(), INTERVAL 1 HOUR) 
        GROUP BY d.dominio, r.tipo_query
        ORDER BY d.dominio');

        $label="Última Hora [1hr]";
        return view('admin.dominios', compact('total_dominios', 'label'));
    }

    public function dominios_7days()
    {
        $total_dominios = DB::select('
        SELECT d.dominio, r.tipo_query, count(*) as cantidad
        FROM registros_dns_query as r 
        INNER JOIN dominios_dataset as d 
        ON r.id_dominio=d.id_dominio 
        AND r.fecha_hr >= DATE_SUB(NOW(), INTERVAL 7 DAY) 
        GROUP BY d.dominio, r.tipo_query
        ORDER BY d.dominio');

        $label="Últimos 7 días";
        return view('admin.dominios', compact('total_dominios', 'label'));
    }

    public function dominios_30days()
    {
        $total_dominios = DB::select('
        SELECT d.dominio, r.tipo_query, count(*) as cantidad
        FROM registros_dns_query as r 
        INNER JOIN dominios_dataset as d 
        ON r.id_dominio=d.id_dominio 
        AND r.fecha_hr >= DATE_SUB(NOW(), INTERVAL 30 DAY) 
        GROUP BY d.dominio, r.tipo_query
        ORDER BY d.dominio');

        $label="Últimos 30 días";
        return view('admin.dominios', compact('total_dominios', 'label'));
    }
}
