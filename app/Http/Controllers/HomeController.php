<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function dashboard()
    {
        //$users = DB::select('select * from users where active = ?', [1]);
        //return view('user.index', ['users' => $users]);
        $total_dns = DB::select('
        SELECT date_format(fecha_hr, "%Y-%m-%d %H:%i") as fecha_hr, count(1) as cantidad 
        FROM registros_dns_query  
        WHERE fecha_hr >= DATE_SUB(NOW(), INTERVAL 1 HOUR) 
        GROUP BY ( 12 * HOUR( fecha_hr ) + FLOOR( MINUTE( fecha_hr ) / 5 )) 
        ORDER BY fecha_hr');

        $botnets = DB::select('
        SELECT date_format(fecha_hr, "%Y-%m-%d %H:%i") as fecha_hr, count(1) as cantidad 
        FROM lista_negra 
        WHERE fecha_hr >= DATE_SUB(NOW(), INTERVAL 1 HOUR) 
        GROUP BY ( 12 * HOUR( fecha_hr ) + FLOOR( MINUTE( fecha_hr ) / 5 )) 
        ORDER BY fecha_hr');

        $lista_negra = DB::select('
        SELECT date_format(fecha_hr, "%Y-%m-%d %H:%i") as fecha_hr, count(1) as cantidad 
        FROM lista_negra_registros 
        WHERE fecha_hr >= DATE_SUB(NOW(), INTERVAL 1 HOUR) 
        GROUP BY ( 12 * HOUR( fecha_hr ) + FLOOR( MINUTE( fecha_hr ) / 5 )) 
        ORDER BY fecha_hr');

        $lista_blanca = DB::select('
        SELECT date_format(fecha_hr, "%Y-%m-%d %H:%i") as fecha_hr, count(1) as cantidad 
        FROM lista_blanca_registros 
        WHERE fecha_hr >= DATE_SUB(NOW(), INTERVAL 1 HOUR) 
        GROUP BY ( 12 * HOUR( fecha_hr ) + FLOOR( MINUTE( fecha_hr ) / 5 )) 
        ORDER BY fecha_hr');
        
        $label="Última Hora [1hr]";
        //return view('admin.dashboard') -> with('total_dns', $total_dns) -> with('botnets', $botnets) -> with('lista_negra', $lista_negra) -> with('lista_blanca', $lista_blanca);
        return view('admin.dashboard', compact('total_dns', 'botnets', 'lista_negra', 'lista_blanca', 'label'));
    }

    public function dashboard_7days()
    {
        $total_dns = DB::select('
        SELECT date_format(fecha_hr, "%Y-%m-%d %H:%i") as fecha_hr, count(1) as cantidad 
        FROM registros_dns_query 
        WHERE fecha_hr >= DATE_SUB(NOW(), INTERVAL 7 DAY) 
        GROUP BY ( 12 * DAY( fecha_hr ) + FLOOR( DAY( fecha_hr ) / 2 )) 
        ORDER BY fecha_hr');

        $botnets = DB::select('
        SELECT date_format(fecha_hr, "%Y-%m-%d %H:%i") as fecha_hr, count(1) as cantidad 
        FROM lista_negra 
        WHERE fecha_hr >= DATE_SUB(NOW(), INTERVAL 7 DAY) 
        GROUP BY ( 12 * DAY( fecha_hr ) + FLOOR( DAY( fecha_hr ) / 2 )) 
        ORDER BY fecha_hr');

        $lista_negra = DB::select('
        SELECT date_format(fecha_hr, "%Y-%m-%d %H:%i") as fecha_hr, count(1) as cantidad 
        FROM lista_negra_registros 
        WHERE fecha_hr >= DATE_SUB(NOW(), INTERVAL 7 DAY) 
        GROUP BY ( 12 * DAY( fecha_hr ) + FLOOR( DAY( fecha_hr ) / 2 )) 
        ORDER BY fecha_hr');

        $lista_blanca = DB::select('
        SELECT date_format(fecha_hr, "%Y-%m-%d %H:%i") as fecha_hr, count(1) as cantidad 
        FROM lista_blanca_registros 
        WHERE fecha_hr >= DATE_SUB(NOW(), INTERVAL 7 DAY) 
        GROUP BY ( 12 * DAY( fecha_hr ) + FLOOR( DAY( fecha_hr ) / 2 )) 
        ORDER BY fecha_hr');

        $label="Últimos 7 días";
        return view('admin.dashboard', compact('total_dns', 'botnets', 'lista_negra', 'lista_blanca', 'label'));
    }

    public function dashboard_30days()
    {
        $total_dns = DB::select('
        SELECT date_format(fecha_hr, "%Y-%m-%d") as fecha_hr, count(1) as cantidad 
        FROM registros_dns_query  
        WHERE fecha_hr >= DATE_SUB(NOW(), INTERVAL 30 DAY) 
        GROUP BY (1) 
        ORDER BY fecha_hr');

        $botnets = DB::select('
        SELECT date_format(fecha_hr, "%Y-%m-%d") as fecha_hr, count(1) as cantidad 
        FROM lista_negra  
        WHERE fecha_hr >= DATE_SUB(NOW(), INTERVAL 30 DAY) 
        GROUP BY (1) 
        ORDER BY fecha_hr');

        $lista_negra = DB::select('
        SELECT date_format(fecha_hr, "%Y-%m-%d") as fecha_hr, count(1) as cantidad 
        FROM lista_negra_registros 
        WHERE fecha_hr >= DATE_SUB(NOW(), INTERVAL 30 DAY) 
        GROUP BY (1) 
        ORDER BY fecha_hr');

        $lista_blanca = DB::select('
        SELECT date_format(fecha_hr, "%Y-%m-%d") as fecha_hr, count(1) as cantidad 
        FROM lista_blanca_registros 
        WHERE fecha_hr >= DATE_SUB(NOW(), INTERVAL 30 DAY) 
        GROUP BY (1) 
        ORDER BY fecha_hr');
        
        $label="Últimos 30 días";
        return view('admin.dashboard', compact('total_dns', 'botnets', 'lista_negra', 'lista_blanca', 'label'));
    }

    /*
    public function alertas()
    {
        return view('admin.alertas');
    }

    public function ln()
    {
        return view('admin.listaNegra');
    }*/
}
