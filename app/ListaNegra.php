<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListaNegra extends Model
{
    //
    protected $table='lista_negra';
    protected $fillable=['dominio','descripcion', 'fecha_hr'];
    protected $primaryKey='id_ln';
    protected $hidden=['id_ln'];
    public $timestamps = false;
}
