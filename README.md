## About

Este proyecto tiene el objetivo de contibruir a la detección de Botnets, mediante el análisis del tráfico de red del Servidor DNS empleando modelos de Machine Learning.
El Dashboard del Frontend, tiene las siguientes funcionalidades.

- Dashboard principal con infografía de las consultas DNS.
- Reporte de Botnets.
- Top Dominios.
- Alertas.
- Lista Blanca de dominios.
- Lista Negra de dominios.


## License
GNU/GPLv3
Dennis Vargas.
